package com.services.readingisgood;

import com.services.readingisgood.api.request.book.BookRequest;
import com.services.readingisgood.api.request.customer.AddressRequest;
import com.services.readingisgood.api.request.customer.CustomerRequest;
import com.services.readingisgood.api.request.order.OrderItemRequest;
import com.services.readingisgood.api.request.order.OrderRequest;
import com.services.readingisgood.domain.book.Book;
import com.services.readingisgood.domain.customer.Address;
import com.services.readingisgood.domain.customer.Customer;
import com.services.readingisgood.domain.order.Order;
import com.services.readingisgood.domain.order.OrderItem;
import com.services.readingisgood.enums.OrderStatus;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;

public class TestBase {

    public static final String BOOK_NAME = "BOOK_NAME";
    public static final String AUTHOR_NAME = "AUTHOR_NAME";
    public static final String NAME = "NAME";
    public static final String ADDRESS = "ADDRESS";
    public static final String COUNTRY = "COUNTRY";
    public static final String CITY = "CITY";
    public static final String DISTRICT = "DISTRICT";
    public static final String STREET = "STREET";
    public static final String ID = "ID";
    public static final String BOOK_ID = "BOOK_ID";
    public static final LocalDate BIRTH_DATE = LocalDate.now().minusYears(30);
    public static final String EMAIL = "test@test.com";
    public static final Integer APARTMENT_NUMBER = 6;
    public static final BigDecimal PRICE = new BigDecimal(100);
    public static final Integer STOCK = 5;
    public static final Integer QUANTITY = 1;

    public Customer prepareCustomer() {
        Customer customer = new Customer();
        customer.setId(ID);
        customer.setFirstName(NAME);
        customer.setLastName(NAME);
        customer.setEmail(EMAIL);
        customer.setOrderList(new ArrayList<>(Arrays.asList(prepareOrder())));
        customer.setBirthDate(BIRTH_DATE);
        customer.setAddress(prepareAddress());

        return customer;
    }

    public Address prepareAddress() {
        Address address = new Address();
        address.setCountry(COUNTRY);
        address.setCity(CITY);
        address.setDistrict(DISTRICT);
        address.setStreet(STREET);
        address.setApartmentNumber(APARTMENT_NUMBER);
        address.setAddress(ADDRESS);

        return address;
    }

    public Order prepareOrder() {
        return Order.builder().ownerFirstName(NAME).ownerLastName(NAME).ownerAddress(ADDRESS)
                .orderStatus(OrderStatus.PLACED).orderItemList(
                        new ArrayList<>(Arrays.asList(prepareOrderItem()))).build();
    }
    public OrderItem prepareOrderItem() {
        return OrderItem.builder().book(prepareBook()).quantity(QUANTITY).build();
    }

    public Book prepareBook() {
        Book book = new Book();
        book.setId(BOOK_ID);
        book.setBookName(BOOK_NAME);
        book.setAuthorName(AUTHOR_NAME);
        book.setPrice(PRICE);
        book.setStock(STOCK);

        return book;
    }

    public CustomerRequest prepareCustomerRequest() {
        CustomerRequest request = new CustomerRequest();
        request.setFirstName(NAME);
        request.setLastName(NAME);
        request.setEmail(EMAIL);
        request.setBirthDate(BIRTH_DATE);
        request.setAddress(prepareAddressRequest());

        return request;
    }

    public AddressRequest prepareAddressRequest() {
        AddressRequest request = new AddressRequest();
        request.setCountry(COUNTRY);
        request.setCity(CITY);
        request.setDistrict(DISTRICT);
        request.setStreet(STREET);
        request.setApartmentNumber(APARTMENT_NUMBER);
        request.setAddress(ADDRESS);

        return request;
    }

    public OrderRequest prepareOrderRequest() {
        OrderRequest request = new OrderRequest();
        request.setCustomerId(ID);
        request.setOrderItemList(new ArrayList<>(Arrays.asList(prepareOrderItemRequest())));

        return request;
    }
    public OrderItemRequest prepareOrderItemRequest() {
        OrderItemRequest request = new OrderItemRequest();
        request.setQuantity(QUANTITY);
        request.setBookId(BOOK_ID);

        return request;
    }

    public BookRequest prepareBookRequest() {
        BookRequest request = new BookRequest();
        request.setBookName(BOOK_NAME);
        request.setAuthorName(AUTHOR_NAME);
        request.setPrice(PRICE);
        request.setStock(STOCK);

        return request;
    }
}
