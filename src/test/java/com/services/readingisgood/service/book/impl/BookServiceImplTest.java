package com.services.readingisgood.service.book.impl;

import com.services.readingisgood.TestBase;
import com.services.readingisgood.api.request.book.BookRequest;
import com.services.readingisgood.domain.book.Book;
import com.services.readingisgood.enums.validation.BookValidationRule;
import com.services.readingisgood.enums.validation.CustomerValidationRule;
import com.services.readingisgood.exception.ValidationException;
import com.services.readingisgood.repository.book.BookRepository;
import com.services.readingisgood.service.book.BookMapper;
import com.services.readingisgood.service.book.BookService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Optional;

class BookServiceImplTest extends TestBase {

    private BookService bookService;

    private BookRepository bookRepository;

    @BeforeEach
    void setUp() {
        bookRepository = Mockito.mock(BookRepository.class);
        bookService = new BookServiceImpl(bookRepository);
    }

    @Test
    void addNewBook() {
        BookRequest request = prepareBookRequest();

        Mockito.when(bookRepository.save(Mockito.any()))
                .thenReturn(BookMapper.INSTANCE.requestToBook(request));

        Book book = bookService.addNewBook(request);

        Assertions.assertEquals(request.getBookName(), book.getBookName());
        Assertions.assertEquals(request.getAuthorName(), book.getAuthorName());
        Assertions.assertEquals(request.getPrice(), book.getPrice());
        Assertions.assertEquals(request.getStock(), book.getStock());
    }

    @Test
    void increaseStock() {
        Book book = prepareBook();
        int firstStockValue = book.getStock();

        Mockito.when(bookRepository.findById(ID)).thenReturn(Optional.of(book));
        Mockito.when(bookRepository.save(Mockito.any()))
                .thenReturn(book);

        Book actual = bookService.increaseStock(ID, QUANTITY);

        Assertions.assertEquals(Integer.sum(firstStockValue, QUANTITY) , actual.getStock());
    }

    @Test
    void getBookById() {
        Mockito.when(bookRepository.findById(ID)).thenReturn(Optional.of(prepareBook()));

        Book book = bookService.getBookById(ID);

        Assertions.assertEquals(BOOK_NAME, book.getBookName());
        Assertions.assertEquals(AUTHOR_NAME, book.getAuthorName());
        Assertions.assertEquals(PRICE, book.getPrice());
        Assertions.assertEquals(STOCK, book.getStock());
    }

    @Test
    void whenGetBookByIdCalledWithNotFoundIdThenShouldThrowException() {
        Mockito.when(bookRepository.findById(ID)).thenReturn(Optional.empty());

        ValidationException exception = Assertions.assertThrows(ValidationException.class, () -> {
            bookService.getBookById(ID);
        });

        Assertions.assertEquals(BookValidationRule.ERR_BOOK_NOT_FOUND, exception.getRule());
    }

}