package com.services.readingisgood.service.order.impl;

import com.services.readingisgood.TestBase;
import com.services.readingisgood.api.request.order.OrderRequest;
import com.services.readingisgood.domain.book.Book;
import com.services.readingisgood.enums.validation.BookValidationRule;
import com.services.readingisgood.enums.validation.CustomerValidationRule;
import com.services.readingisgood.enums.validation.OrderValidationRule;
import com.services.readingisgood.exception.ValidationException;
import com.services.readingisgood.service.book.BookService;
import com.services.readingisgood.service.order.OrderValidationService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class OrderValidationServiceImplTest extends TestBase {

    private OrderValidationService orderValidationService;

    private BookService bookService;

    @BeforeEach
    void setUp() {
        bookService = Mockito.mock(BookService.class);
        orderValidationService = new OrderValidationServiceImpl(bookService);
    }

    @Test
    void validateOrderRequest() {
        OrderRequest request = prepareOrderRequest();

        List<Book> bookList = Arrays.asList(prepareBook());
        int firstStock = bookList.get(0).getStock();

        Mockito.when(bookService.getBooksByIdList(Mockito.anyList())).thenReturn(bookList);

        List<Book> actualList = orderValidationService.validateOrderRequest(request);

        Assertions.assertEquals(firstStock - request.getOrderItemList().get(0).getQuantity(),
                actualList.get(0).getStock());
    }

    @Test
    void whenValidateOrderRequestCalledWithNotFoundIdThenShouldThrowException() {
        OrderRequest request = prepareOrderRequest();

        Mockito.when(bookService.getBooksByIdList(Mockito.anyList())).thenReturn(new ArrayList<>());

        ValidationException exception = Assertions.assertThrows(ValidationException.class, () -> {
            orderValidationService.validateOrderRequest(request);
        });

        Assertions.assertEquals(BookValidationRule.ERR_BOOK_NOT_FOUND, exception.getRule());
    }

    @Test
    void whenValidateOrderRequestCalledAndStockInsufficientThenShouldThrowException() {
        OrderRequest request = prepareOrderRequest();
        request.getOrderItemList().get(0).setQuantity(100);

        List<Book> bookList = Arrays.asList(prepareBook());
        int firstStock = bookList.get(0).getStock();

        Mockito.when(bookService.getBooksByIdList(Mockito.anyList())).thenReturn(bookList);

        ValidationException exception = Assertions.assertThrows(ValidationException.class, () -> {
            orderValidationService.validateOrderRequest(request);
        });

        Assertions.assertEquals(OrderValidationRule.ERR_STOCK_INSUFFICIENT, exception.getRule());
    }
}