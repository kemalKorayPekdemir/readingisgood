package com.services.readingisgood.service.order.impl;

import com.services.readingisgood.TestBase;
import com.services.readingisgood.api.request.order.OrderRequest;
import com.services.readingisgood.domain.customer.Customer;
import com.services.readingisgood.domain.order.Order;
import com.services.readingisgood.enums.OrderStatus;
import com.services.readingisgood.enums.validation.CustomerValidationRule;
import com.services.readingisgood.enums.validation.OrderValidationRule;
import com.services.readingisgood.exception.ValidationException;
import com.services.readingisgood.repository.order.OrderItemRepository;
import com.services.readingisgood.repository.order.OrderRepository;
import com.services.readingisgood.service.book.BookService;
import com.services.readingisgood.service.customer.CustomerMapper;
import com.services.readingisgood.service.customer.CustomerService;
import com.services.readingisgood.service.order.OrderMapper;
import com.services.readingisgood.service.order.OrderService;
import com.services.readingisgood.service.order.OrderValidationService;
import com.services.readingisgood.service.order.helper.OrderStatusHelper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class OrderServiceImplTest extends TestBase {

    private OrderService orderService;

    private OrderRepository orderRepository;

    private OrderItemRepository orderItemRepository;

    private CustomerService customerService;

    private BookService bookService;

    private OrderStatusHelper orderStatusHelper;

    private OrderValidationService orderValidationService;

    @BeforeEach
    void setUp() {
        orderRepository = Mockito.mock(OrderRepository.class);
        orderItemRepository = Mockito.mock(OrderItemRepository.class);
        customerService = Mockito.mock(CustomerService.class);
        bookService = Mockito.mock(BookService.class);
        orderValidationService = Mockito.mock(OrderValidationService.class);
        orderStatusHelper = new OrderStatusHelper();
        orderService = new OrderServiceImpl(orderRepository, orderItemRepository, customerService, bookService,
                orderStatusHelper, orderValidationService);
    }

    @Test
    void placeOrder() {
        OrderRequest request = prepareOrderRequest();
        Customer customer = prepareCustomer();

        Mockito.when(customerService.getCustomerById(request.getCustomerId())).thenReturn(customer);
        Mockito.when(orderValidationService.validateOrderRequest(request)).thenReturn(new ArrayList<>());
        Mockito.when(bookService.getBookById(Mockito.anyString())).thenReturn(prepareBook());

        orderService.placeOrder(request);

        Mockito.verify(orderItemRepository, Mockito.times(1)).saveAll(Mockito.any());
        Mockito.verify(orderRepository, Mockito.times(1)).save(Mockito.any());
        Mockito.verify(bookService, Mockito.times(1)).saveBookList(Mockito.any());
        Mockito.verify(customerService, Mockito.times(1)).saveCustomer(Mockito.any());
    }

    @Test
    void getOrderById() {
        Mockito.when(orderRepository.findById(ID))
                .thenReturn(Optional.of(prepareOrder()));

        Order order = orderService.getOrderById(ID);

        Assertions.assertEquals(NAME, order.getOwnerFirstName());
        Assertions.assertEquals(NAME, order.getOwnerLastName());
        Assertions.assertEquals(ADDRESS, order.getOwnerAddress());
        Assertions.assertEquals(1, order.getOrderItemList().size());
        Assertions.assertEquals(OrderStatus.PLACED, order.getOrderStatus());
    }

    @Test
    void whenGetOrderByIdCalledWithNotFoundIdThenShouldThrowException() {
        Mockito.when(orderRepository.findById(ID))
                .thenReturn(Optional.empty());

        ValidationException exception = Assertions.assertThrows(ValidationException.class, () -> {
            orderService.getOrderById(ID);
        });

        Assertions.assertEquals(OrderValidationRule.ERR_ORDER_NOT_FOUND, exception.getRule());
    }

    @Test
    void advanceOrderStatus() {
        Order order = prepareOrder();

        Mockito.when(orderRepository.findById(ID))
                .thenReturn(Optional.of(order));
        Mockito.when(orderRepository.save(order))
                .thenReturn(order);

        Order actual = orderService.advanceOrderStatus(ID);

        Assertions.assertEquals(OrderStatus.ON_DELIVERY, actual.getOrderStatus());
    }

}