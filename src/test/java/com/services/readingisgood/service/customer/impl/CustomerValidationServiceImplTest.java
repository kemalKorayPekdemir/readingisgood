package com.services.readingisgood.service.customer.impl;

import com.services.readingisgood.TestBase;
import com.services.readingisgood.api.request.customer.CustomerRequest;
import com.services.readingisgood.enums.validation.CustomerValidationRule;
import com.services.readingisgood.enums.validation.OrderValidationRule;
import com.services.readingisgood.exception.ValidationException;
import com.services.readingisgood.repository.customer.CustomerRepository;
import com.services.readingisgood.service.customer.CustomerValidationService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.*;

class CustomerValidationServiceImplTest extends TestBase {

    private CustomerValidationService customerValidationService;

    private CustomerRepository customerRepository;

    @BeforeEach
    void setUp() {
        customerRepository = Mockito.mock(CustomerRepository.class);
        customerValidationService = new CustomerValidationServiceImpl(customerRepository);
    }

    @Test
    void validateCustomerRequest() {
        CustomerRequest request = prepareCustomerRequest();

        Mockito.when(customerRepository.findByEmail(request.getEmail())).thenReturn(prepareCustomer());

        ValidationException exception = Assertions.assertThrows(ValidationException.class, () -> {
            customerValidationService.validateCustomerRequest(request);
        });

        Assertions.assertEquals(CustomerValidationRule.ERR_MAIL_ALREADY_USED, exception.getRule());
    }
}