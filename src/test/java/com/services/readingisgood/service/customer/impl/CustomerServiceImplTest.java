package com.services.readingisgood.service.customer.impl;

import com.services.readingisgood.TestBase;
import com.services.readingisgood.api.request.customer.CustomerRequest;
import com.services.readingisgood.domain.customer.Customer;
import com.services.readingisgood.domain.order.Order;
import com.services.readingisgood.enums.validation.CustomerValidationRule;
import com.services.readingisgood.exception.ValidationException;
import com.services.readingisgood.repository.customer.CustomerRepository;
import com.services.readingisgood.service.customer.CustomerMapper;
import com.services.readingisgood.service.customer.CustomerService;
import com.services.readingisgood.service.customer.CustomerValidationService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.List;
import java.util.Optional;

class CustomerServiceImplTest extends TestBase {

    private CustomerService customerService;

    private  CustomerRepository customerRepository;

    private  CustomerValidationService customerValidationService;

    @BeforeEach
    void setUp() {
        customerRepository = Mockito.mock(CustomerRepository.class);
        customerValidationService = Mockito.mock(CustomerValidationService.class);
        customerService = new CustomerServiceImpl(customerRepository, customerValidationService);
    }

    @Test
    void getCustomerById() {
        Mockito.when(customerRepository.findById(ID))
                .thenReturn(Optional.of(prepareCustomer()));

        Customer customer = customerService.getCustomerById(ID);

        Assertions.assertEquals(ID, customer.getId());
        Assertions.assertEquals(NAME, customer.getFirstName());
        Assertions.assertEquals(EMAIL, customer.getEmail());
        Assertions.assertEquals(1, customer.getOrderList().size());
        Assertions.assertNotNull(customer.getAddress());
    }

    @Test
    void whenGetCustomerByIdCalledWithNotFoundIdThenShouldThrowException() {
        Mockito.when(customerRepository.findById(ID))
                .thenReturn(Optional.empty());

        ValidationException exception = Assertions.assertThrows(ValidationException.class, () -> {
            customerService.getCustomerById(ID);
        });

        Assertions.assertEquals(CustomerValidationRule.ERR_CUSTOMER_NOT_FOUND, exception.getRule());
    }

    @Test
    public void createCustomer() {
        CustomerRequest request = prepareCustomerRequest();

        Mockito.when(customerRepository.save(Mockito.any()))
                .thenReturn(CustomerMapper.INSTANCE.requestToCustomer(request));

        Customer customer = customerService.createCustomer(request);

        Assertions.assertEquals(request.getFirstName(), customer.getFirstName());
        Assertions.assertEquals(request.getEmail(), customer.getEmail());
        Assertions.assertEquals(request.getLastName(), customer.getLastName());
        Assertions.assertEquals(request.getBirthDate(), customer.getBirthDate());
    }

    @Test
    public void getCustomerOrdersById() {
        Customer customer = prepareCustomer();
        Mockito.when(customerRepository.findById(ID))
                .thenReturn(Optional.of(customer));

        List<Order> actual = customerService.getCustomerOrdersById(ID);

        Assertions.assertEquals(customer.getOrderList(), actual);
    }

}