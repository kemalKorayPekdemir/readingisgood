package com.services.readingisgood.service.statistics;

import com.services.readingisgood.api.response.statistichs.MonthlyStatisticsResponse;

import java.util.List;

public interface StatisticsService {

    List<MonthlyStatisticsResponse> getMonthlyStatistics(String id);
}
