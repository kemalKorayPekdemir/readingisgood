package com.services.readingisgood.service.statistics.impl;

import com.services.readingisgood.api.response.statistichs.MonthlyStatisticsResponse;
import com.services.readingisgood.domain.order.Order;
import com.services.readingisgood.domain.order.OrderItem;
import com.services.readingisgood.service.customer.CustomerService;
import com.services.readingisgood.service.order.OrderService;
import com.services.readingisgood.service.statistics.StatisticsService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class StatisticsServiceImpl implements StatisticsService {

    private final CustomerService customerService;

    private final OrderService orderService;

    public List<MonthlyStatisticsResponse> getMonthlyStatistics(String id) {
        List<Order> orderList = customerService.getCustomerOrdersById(id).stream()
                .sorted(Comparator.comparing(Order::getCreatedDate)).collect(Collectors.toList());

        if (orderList.isEmpty()) {
            return new ArrayList<>();
        }

        LocalDate firstOrderDate = orderList.get(0).getCreatedDate().toLocalDate();
        LocalDate lastOrderDate = orderList.get(orderList.size() - 1).getCreatedDate().toLocalDate().withDayOfMonth(1);

        LocalDate currentDate = LocalDate.of(firstOrderDate.getYear(), firstOrderDate.getMonth(), 1);

        List<MonthlyStatisticsResponse> monthlyStatistics =  new ArrayList<>();
        while(!currentDate.isAfter(lastOrderDate)) {
            monthlyStatistics.add(prepareMonthlyStatistics(orderService
                    .getOrdersBetweenDateInterval(currentDate, currentDate.plusMonths(1L)), currentDate));

            currentDate = currentDate.plusMonths(1L);
        }

        return monthlyStatistics;
    }

    private MonthlyStatisticsResponse prepareMonthlyStatistics(List<Order> orderList, LocalDate currentDate) {
        String month = currentDate.getMonth().name() + " " + currentDate.getYear();

        if (orderList.isEmpty()) {
            return new MonthlyStatisticsResponse(month, 0, 0, BigDecimal.ZERO);
        }

        Integer totalBookCount = 0;
        BigDecimal totalAmount = BigDecimal.ZERO;
        for (Order order : orderList) {
            totalBookCount = Integer.sum(totalBookCount, order.getOrderItemList().stream()
                    .mapToInt(OrderItem::getQuantity).sum());
            totalAmount = totalAmount.add(order.getOrderItemList().stream().map(o -> o.getBook().getPrice())
                    .reduce(BigDecimal.ZERO, BigDecimal::add));
        }

        return new MonthlyStatisticsResponse(month, orderList.size(), totalBookCount, totalAmount);
    }
}
