package com.services.readingisgood.service.customer;

import com.services.readingisgood.api.request.customer.CustomerRequest;
import com.services.readingisgood.domain.customer.Customer;
import com.services.readingisgood.domain.order.Order;
import com.services.readingisgood.dto.customer.CustomerDTO;

import java.util.List;

public interface CustomerService {

    Customer createCustomer(CustomerRequest request);

    Customer getCustomerById(String id);

    Customer saveCustomer(Customer customer);

    List<Order> getCustomerOrdersById(String id);

}
