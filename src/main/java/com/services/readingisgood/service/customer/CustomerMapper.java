package com.services.readingisgood.service.customer;

import com.services.readingisgood.api.request.customer.CustomerRequest;
import com.services.readingisgood.domain.customer.Customer;
import com.services.readingisgood.dto.customer.CustomerDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface CustomerMapper {

    CustomerMapper INSTANCE = Mappers.getMapper(CustomerMapper.class);

    Customer requestToCustomer(CustomerRequest request);

    CustomerDTO customerToDTO(Customer customer);
}
