package com.services.readingisgood.service.customer.impl;

import com.services.readingisgood.api.request.customer.CustomerRequest;
import com.services.readingisgood.domain.customer.Customer;
import com.services.readingisgood.domain.order.Order;
import com.services.readingisgood.enums.validation.CustomerValidationRule;
import com.services.readingisgood.exception.ValidationException;
import com.services.readingisgood.repository.customer.CustomerRepository;
import com.services.readingisgood.service.customer.CustomerMapper;
import com.services.readingisgood.service.customer.CustomerService;
import com.services.readingisgood.service.customer.CustomerValidationService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CustomerServiceImpl  implements CustomerService {

    private final CustomerRepository customerRepository;

    private final CustomerValidationService customerValidationService;

    @Override
    @Transactional
    public Customer createCustomer(CustomerRequest request) {
        customerValidationService.validateCustomerRequest(request);
        return saveCustomer(CustomerMapper.INSTANCE.requestToCustomer(request));
    }

    @Override
    public Customer getCustomerById(String id) {
        return customerRepository.findById(id).orElseThrow(
                () -> new ValidationException(CustomerValidationRule.ERR_CUSTOMER_NOT_FOUND));
    }

    @Override
    @Transactional
    public Customer saveCustomer(Customer customer) {
        return customerRepository.save(customer);
    }

    @Override
    public List<Order> getCustomerOrdersById(String id) {
        Customer customer = getCustomerById(id);

        return customer.getOrderList();
    }

}
