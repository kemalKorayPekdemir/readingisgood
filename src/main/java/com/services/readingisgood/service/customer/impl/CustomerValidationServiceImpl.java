package com.services.readingisgood.service.customer.impl;

import com.services.readingisgood.api.request.customer.CustomerRequest;
import com.services.readingisgood.domain.customer.Customer;
import com.services.readingisgood.enums.validation.CustomerValidationRule;
import com.services.readingisgood.exception.ValidationException;
import com.services.readingisgood.repository.customer.CustomerRepository;
import com.services.readingisgood.service.customer.CustomerValidationService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CustomerValidationServiceImpl implements CustomerValidationService {

    private final CustomerRepository customerRepository;

    @Override
    public void validateCustomerRequest(CustomerRequest request) {
        Customer customer = customerRepository.findByEmail(request.getEmail());

        if (customer != null) {
            throw new ValidationException(CustomerValidationRule.ERR_MAIL_ALREADY_USED);
        }
    }
}
