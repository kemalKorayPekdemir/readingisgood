package com.services.readingisgood.service.customer;

import com.services.readingisgood.api.request.customer.CustomerRequest;

public interface CustomerValidationService {

    void validateCustomerRequest(CustomerRequest request);
}
