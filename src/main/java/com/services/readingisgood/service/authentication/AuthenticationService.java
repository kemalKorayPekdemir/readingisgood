package com.services.readingisgood.service.authentication;

public interface AuthenticationService {

    void authenticate(String username, String password);
}
