package com.services.readingisgood.service.order.impl;

import com.services.readingisgood.api.request.order.OrderItemRequest;
import com.services.readingisgood.api.request.order.OrderRequest;
import com.services.readingisgood.domain.book.Book;
import com.services.readingisgood.enums.validation.BookValidationRule;
import com.services.readingisgood.enums.validation.OrderValidationRule;
import com.services.readingisgood.exception.ValidationException;
import com.services.readingisgood.service.book.BookService;
import com.services.readingisgood.service.order.OrderValidationService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class OrderValidationServiceImpl implements OrderValidationService {

    private final BookService bookService;

    @Override
    public List<Book> validateOrderRequest(OrderRequest request) {
        List<OrderItemRequest> orderItemRequestList = request.getOrderItemList();
        List<Book> bookList = bookService.getBooksByIdList(orderItemRequestList.stream()
                .map(OrderItemRequest::getBookId).collect(Collectors.toList()));

        Map<String, Integer> map = new HashMap<>();
        for (OrderItemRequest orderItemRequest : orderItemRequestList) {
            if (map.get(orderItemRequest.getBookId()) == null) {
                map.put(orderItemRequest.getBookId(), orderItemRequest.getQuantity());
            } else {
                map.put(orderItemRequest.getBookId(), Integer.sum(map.get(orderItemRequest.getBookId()),
                        orderItemRequest.getQuantity()));
            }
        }

        if (map.size() != bookList.size()) {
            throw new ValidationException(BookValidationRule.ERR_BOOK_NOT_FOUND);
        }

        return checkStockAndReturnBookList(bookList, map);
    }

    private List<Book> checkStockAndReturnBookList(List<Book> bookList, Map<String, Integer> map) {
        for (Book book : bookList) {
            Integer finalStock = book.getStock() - map.get(book.getId());
            if (Integer.signum(finalStock) == -1) {
                throw new ValidationException(OrderValidationRule.ERR_STOCK_INSUFFICIENT);
            }
            book.setStock(finalStock);
            book.setLastUpdatedDate(LocalDateTime.now());
        }

        return bookList;
    }

}
