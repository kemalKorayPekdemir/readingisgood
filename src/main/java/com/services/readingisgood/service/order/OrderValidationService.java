package com.services.readingisgood.service.order;

import com.services.readingisgood.api.request.order.OrderRequest;
import com.services.readingisgood.domain.book.Book;

import java.util.List;

public interface OrderValidationService {

    List<Book> validateOrderRequest(OrderRequest request);
}
