package com.services.readingisgood.service.order;

import com.services.readingisgood.domain.order.Order;
import com.services.readingisgood.dto.order.OrderDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface OrderMapper {

    OrderMapper INSTANCE = Mappers.getMapper(OrderMapper.class);

    List<OrderDTO> orderListToOrderDTOList(List<Order> orderList);

    OrderDTO orderDocumentToOrderDTO(Order order);
}
