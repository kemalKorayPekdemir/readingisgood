package com.services.readingisgood.service.order;

import com.services.readingisgood.api.request.order.OrderRequest;
import com.services.readingisgood.api.request.order.QueryOrderRequest;
import com.services.readingisgood.domain.order.Order;

import java.time.LocalDate;
import java.util.List;

public interface OrderService {

    Order placeOrder(OrderRequest request);

    Order getOrderById(String id);

    Order advanceOrderStatus(String orderId);

    List<Order> getOrdersBetweenDateInterval(LocalDate startDate, LocalDate endDate);
}
