package com.services.readingisgood.service.order.helper;

import com.services.readingisgood.enums.OrderStatus;
import com.services.readingisgood.enums.validation.OrderValidationRule;
import com.services.readingisgood.exception.ValidationException;
import org.springframework.stereotype.Component;

@Component
public class OrderStatusHelper {

    public OrderStatus getNextStatus(OrderStatus orderStatus) {
        if (orderStatus.equals(OrderStatus.PLACED)) {
            orderStatus = OrderStatus.ON_DELIVERY;
        } else if (orderStatus.equals(OrderStatus.ON_DELIVERY)) {
            orderStatus = OrderStatus.DELIVERED;
        } else if (orderStatus.equals(OrderStatus.DELIVERED)) {
            throw new ValidationException(OrderValidationRule.ERR_ORDER_ALREADY_DELIVERED);
        }

        return orderStatus;
    }
}
