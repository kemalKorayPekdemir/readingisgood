package com.services.readingisgood.service.order.impl;

import com.services.readingisgood.api.request.order.OrderItemRequest;
import com.services.readingisgood.api.request.order.OrderRequest;
import com.services.readingisgood.domain.book.Book;
import com.services.readingisgood.domain.customer.Customer;
import com.services.readingisgood.domain.order.Order;
import com.services.readingisgood.domain.order.OrderItem;
import com.services.readingisgood.enums.OrderStatus;
import com.services.readingisgood.enums.validation.OrderValidationRule;
import com.services.readingisgood.exception.ValidationException;
import com.services.readingisgood.repository.order.OrderItemRepository;
import com.services.readingisgood.repository.order.OrderRepository;
import com.services.readingisgood.service.book.BookService;
import com.services.readingisgood.service.customer.CustomerService;
import com.services.readingisgood.service.order.OrderService;
import com.services.readingisgood.service.order.OrderValidationService;
import com.services.readingisgood.service.order.helper.OrderStatusHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService {

    private final OrderRepository orderRepository;

    private final OrderItemRepository orderItemRepository;

    private final CustomerService customerService;

    private final BookService bookService;

    private final OrderStatusHelper orderStatusHelper;

    private final OrderValidationService orderValidationService;

    @Transactional
    @Override
    public Order placeOrder(OrderRequest request) {
        Customer customer = customerService.getCustomerById(request.getCustomerId());
        List<Book> bookList = orderValidationService.validateOrderRequest(request);

        List<OrderItem> orderItemList = prepareOrderItemList(request.getOrderItemList());
        Order order = Order.builder().ownerFirstName(customer.getFirstName())
                .ownerLastName(customer.getLastName()).ownerAddress(customer.getAddress().getAddress())
                .orderItemList(orderItemList).orderStatus(OrderStatus.PLACED)
                .build();

        customer.addOrder(order);

        orderItemRepository.saveAll(orderItemList);
        Order createdOrder = orderRepository.save(order);
        bookService.saveBookList(bookList);
        customerService.saveCustomer(customer);

        return createdOrder;
    }

    private List<OrderItem> prepareOrderItemList(List<OrderItemRequest> orderItemList) {
        return orderItemList.stream().map(o -> OrderItem.builder().book(bookService.getBookById(o.getBookId()))
                .quantity(o.getQuantity()).build()).collect(Collectors.toList());
    }

    @Override
    public Order getOrderById(String id) {
        return orderRepository.findById(id).orElseThrow(
                () -> new ValidationException(OrderValidationRule.ERR_ORDER_NOT_FOUND));
    }

    @Override
    @Transactional
    public Order advanceOrderStatus(String orderId) {
        Order order = getOrderById(orderId);

        order.setOrderStatus(orderStatusHelper.getNextStatus(order.getOrderStatus()));
        order.setLastUpdatedDate(LocalDateTime.now());

        return orderRepository.save(order);
    }

    @Override
    public List<Order> getOrdersBetweenDateInterval(LocalDate startDate, LocalDate endDate) {
        return orderRepository.findAllByCreatedDateBetween(startDate, endDate);
    }
}
