package com.services.readingisgood.service.book;

import com.services.readingisgood.api.request.book.BookRequest;
import com.services.readingisgood.domain.book.Book;

import java.util.List;

public interface BookService {

    Book addNewBook(BookRequest request);

    void saveBookList(List<Book> book);

    Book increaseStock(String id, Integer number);

    Book getBookById(String id);

    List<Book> getAllBooks();

    List<Book> getBooksByIdList(List<String> idList);


}
