package com.services.readingisgood.service.book.impl;

import com.services.readingisgood.api.request.book.BookRequest;
import com.services.readingisgood.domain.book.Book;
import com.services.readingisgood.enums.validation.BookValidationRule;
import com.services.readingisgood.exception.ValidationException;
import com.services.readingisgood.repository.book.BookRepository;
import com.services.readingisgood.service.book.BookMapper;
import com.services.readingisgood.service.book.BookService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

@Service
@RequiredArgsConstructor
public class BookServiceImpl implements BookService {

    private final BookRepository bookRepository;

    @Override
    @Transactional
    public Book addNewBook(BookRequest request) {
        return bookRepository.save(BookMapper.INSTANCE.requestToBook(request));
    }

    @Transactional
    @Override
    public void saveBookList(List<Book> bookList) {
        bookRepository.saveAll(bookList);
    }

    @Override
    @Transactional
    public Book increaseStock(String id, Integer number) {
        Book book = getBookById(id);
        book.increaseStock(number);
        book.setLastUpdatedDate(LocalDateTime.now());
        return bookRepository.save(book);
    }

    @Override
    public Book getBookById(String id) {
        return bookRepository.findById(id).orElseThrow(
                () -> new ValidationException(BookValidationRule.ERR_BOOK_NOT_FOUND));
    }

    @Override
    public List<Book> getAllBooks() {
        return bookRepository.findAll();
    }

    @Override
    public List<Book> getBooksByIdList(List<String> idList) {
        return bookRepository.findByIdIn(idList);
    }
}
