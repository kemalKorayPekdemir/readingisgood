package com.services.readingisgood.service.book;

import com.services.readingisgood.api.request.book.BookRequest;
import com.services.readingisgood.dto.book.BookDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import com.services.readingisgood.domain.book.Book;

import java.util.List;

@Mapper
public interface BookMapper {

    BookMapper INSTANCE = Mappers.getMapper(BookMapper.class);

    Book requestToBook(BookRequest request);

    BookDTO bookToBookDTO(Book book);

    List<BookDTO> bookListToBookDTOList(List<Book> bookList);

}
