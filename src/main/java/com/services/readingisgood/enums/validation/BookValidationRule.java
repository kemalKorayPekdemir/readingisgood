package com.services.readingisgood.enums.validation;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum BookValidationRule implements ValidationRule {

    ERR_BOOK_NOT_FOUND("ERR_BOOK_NOT_FOUND", "Book is not found.");

    private final String code;

    private final String description;


}
