package com.services.readingisgood.enums.validation;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum OrderValidationRule implements ValidationRule{

    ERR_STOCK_INSUFFICIENT("ERR_STOCK_INSUFFICIENT", "Not enough items in the stock."),
    ERR_ORDER_NOT_FOUND("ERR_ORDER_NOT_FOUND", "Order is not found."),
    ERR_ORDER_ALREADY_DELIVERED("ERR_ORDER_ALREADY_DELIVERED", "Order is already delivered.");

    private final String code;

    private final String description;

}
