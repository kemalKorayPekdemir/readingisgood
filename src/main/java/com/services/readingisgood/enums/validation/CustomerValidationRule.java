package com.services.readingisgood.enums.validation;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum CustomerValidationRule implements ValidationRule {

    ERR_CUSTOMER_NOT_FOUND("ERR_CUSTOMER_NOT_FOUND", "Customer is not found."),
    ERR_MAIL_ALREADY_USED("ERR_MAIL_ALREADY_USED", "This mail is already used.");

    private final String code;

    private final String description;

}
