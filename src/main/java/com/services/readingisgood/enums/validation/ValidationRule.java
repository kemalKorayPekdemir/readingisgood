package com.services.readingisgood.enums.validation;

public interface ValidationRule {

    String getCode();

    String getDescription();
}
