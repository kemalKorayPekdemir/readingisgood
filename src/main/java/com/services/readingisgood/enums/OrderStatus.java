package com.services.readingisgood.enums;

public enum OrderStatus {
    PLACED,
    ON_DELIVERY,
    DELIVERED
}
