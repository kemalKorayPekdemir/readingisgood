package com.services.readingisgood.exception;

import com.services.readingisgood.enums.validation.ValidationRule;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class ValidationException extends RuntimeException{

    private final ValidationRule rule;
}
