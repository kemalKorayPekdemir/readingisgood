package com.services.readingisgood.repository.order;

import com.services.readingisgood.domain.order.Order;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.time.LocalDate;
import java.util.List;

public interface OrderRepository extends MongoRepository<Order, String> {

    List<Order> findAllByCreatedDateBetween(LocalDate startDate, LocalDate endDate);
}
