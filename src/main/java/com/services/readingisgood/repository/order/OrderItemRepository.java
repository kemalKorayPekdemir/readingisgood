package com.services.readingisgood.repository.order;

import com.services.readingisgood.domain.order.OrderItem;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface OrderItemRepository extends MongoRepository<OrderItem, String> {
}
