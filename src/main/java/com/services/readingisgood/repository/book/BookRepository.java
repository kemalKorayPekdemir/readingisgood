package com.services.readingisgood.repository.book;


import org.springframework.data.mongodb.repository.MongoRepository;

import com.services.readingisgood.domain.book.Book;
import java.util.List;

public interface BookRepository extends MongoRepository<Book, String> {

    List<Book> findByIdIn(List<String> bookCodeList);

}
