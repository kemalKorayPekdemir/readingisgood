package com.services.readingisgood.api.response.statistichs;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ApiModel
public class MonthlyStatisticsResponse {

    @ApiModelProperty(required = true, value = "month", example = "May 2021",
            dataType = "String")
    @NotBlank
    private String month;

    @ApiModelProperty(required = true, value = "total order count of the customer", example = "2",
            dataType = "Integer")
    @NotNull
    private Integer totalOrderCount;

    @ApiModelProperty(required = true, value = "total book count of the customer", example = "5",
            dataType = "Integer")
    @NotNull
    private Integer totalBookCount;

    @ApiModelProperty(required = true, value = "total purchased amount", example = "100",
            dataType = "BigDecimal")
    @Min(value = 0)
    @NotNull
    private BigDecimal totalPurchasedAmount;


}
