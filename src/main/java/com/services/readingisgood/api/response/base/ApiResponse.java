package com.services.readingisgood.api.response.base;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter

public class ApiResponse<O> {

    private Boolean success;

    private O result;

    private String errorCode;

    private String errorMessage;

    public ApiResponse(Boolean success, O result) {
        this.success = success;
        this.result = result;
    }

    public ApiResponse(Boolean success, O result, String errorCode, String errorMessage) {
        this.success = success;
        this.result = result;
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }
}
