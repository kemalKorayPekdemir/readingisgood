package com.services.readingisgood.api.controller.statistics;

import com.services.readingisgood.api.constants.ApiEndpoints;
import com.services.readingisgood.api.response.base.ApiResponse;
import com.services.readingisgood.api.response.statistichs.MonthlyStatisticsResponse;
import com.services.readingisgood.service.statistics.StatisticsService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping(ApiEndpoints.STATISTICS_API_URL)
public class StatisticsController {

    private final StatisticsService statisticsService;

    @GetMapping(value = "/{id}/monthly")
    @ApiOperation(value = "Get customer's monthly statistics")
    public ApiResponse<List<MonthlyStatisticsResponse>> getMonthlyStatistics(@ApiParam(name = "id", type = "String",
            value = "id of the customer", required = true) @PathVariable("id") String id) {
        return new ApiResponse<>(true, statisticsService.getMonthlyStatistics(id));
    }
}
