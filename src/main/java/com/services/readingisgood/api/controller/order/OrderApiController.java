package com.services.readingisgood.api.controller.order;

import com.services.readingisgood.api.constants.ApiEndpoints;
import com.services.readingisgood.api.request.order.OrderRequest;
import com.services.readingisgood.api.response.base.ApiResponse;
import com.services.readingisgood.dto.order.OrderDTO;
import com.services.readingisgood.service.order.OrderMapper;
import com.services.readingisgood.service.order.OrderService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping(ApiEndpoints.ORDER_API_URL)
public class OrderApiController {

    private final OrderService orderService;

    @PostMapping(value = "/", consumes = {MediaType.APPLICATION_JSON_VALUE})
    @ApiOperation(value = "place order")
    public ApiResponse<OrderDTO> placeOrder(@ApiParam(name = "request", type = "OrderRequest",
            value = "place order request") @Valid @RequestBody OrderRequest request) {
        return new ApiResponse<>(true, OrderMapper.INSTANCE.orderDocumentToOrderDTO(orderService
                .placeOrder(request))) ;
    }

    @GetMapping(value = "/{id}")
    @ApiOperation(value = "get order by id")
    public ApiResponse<OrderDTO> getOrderById(@ApiParam(name = "id", type = "String",
            value = "id of the order", required = true) @PathVariable("id") String id) {
        return new ApiResponse<>(true, OrderMapper.INSTANCE.orderDocumentToOrderDTO(orderService
                .getOrderById(id))) ;
    }

    @PatchMapping(value = "/{id}/advanceorderstatus", consumes = {MediaType.ALL_VALUE})
    @ApiOperation(value = "advance order status of the order")
    public ApiResponse<OrderDTO> advanceOrderStatus(@ApiParam(name = "id", type = "String", value = "id of the order",
            required = true) @PathVariable("id") String id) {
        return new ApiResponse<>(true, OrderMapper.INSTANCE.orderDocumentToOrderDTO(orderService
                .advanceOrderStatus(id)));
    }

    @GetMapping(value = "query/dateintervals", consumes = {MediaType.ALL_VALUE})
    @ApiOperation(value = "finds order between given date intervals")
    public ApiResponse<List<OrderDTO>> getOrdersBetweenDateInterval(
            @RequestParam(value = "startDate", required = true)
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
            @RequestParam(value = "endDate", required = true)
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate) {
        return new ApiResponse<>(true, OrderMapper.INSTANCE.orderListToOrderDTOList(orderService
                .getOrdersBetweenDateInterval(startDate, endDate)));
    }


}
