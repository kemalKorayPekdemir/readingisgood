package com.services.readingisgood.api.controller.customer;

import com.services.readingisgood.api.constants.ApiEndpoints;
import com.services.readingisgood.api.request.customer.CustomerRequest;
import com.services.readingisgood.api.response.base.ApiResponse;
import com.services.readingisgood.dto.customer.CustomerDTO;
import com.services.readingisgood.dto.order.OrderDTO;
import com.services.readingisgood.service.customer.CustomerMapper;
import com.services.readingisgood.service.customer.CustomerService;
import com.services.readingisgood.service.order.OrderMapper;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping(ApiEndpoints.CUSTOMER_API_URL)
public class CustomerApiController {

    private final CustomerService customerService;

    @PostMapping(value = "/", consumes = {MediaType.APPLICATION_JSON_VALUE})
    @ApiOperation(value = "create new customer")
    public ApiResponse<CustomerDTO> createCustomer(@ApiParam(name = "request", type = "CustomerRequest",
            value = "create new customer request") @Valid @RequestBody CustomerRequest request) {
        return new ApiResponse<>(true, CustomerMapper.INSTANCE
                .customerToDTO(customerService.createCustomer(request)));
    }

    @GetMapping(value = "/{id}")
    @ApiOperation(value = "get customer by id")
    public ApiResponse<CustomerDTO> getCustomerById(@ApiParam(name = "id", type = "String",
            value = "id of the customer", required = true) @PathVariable("id") String id) {
        return new ApiResponse<>(true, CustomerMapper.INSTANCE
                .customerToDTO(customerService.getCustomerById(id))) ;
    }

    @GetMapping(value = "/{id}/orders")
    @ApiOperation(value = "get customer orders by customer id")
    public ApiResponse<List<OrderDTO>> getCustomerOrdersById(@ApiParam(name = "id", type = "String",
            value = "id of the customer", required = true) @PathVariable("id") String id) {
        return new ApiResponse<>(true, OrderMapper.INSTANCE.orderListToOrderDTOList(customerService
                .getCustomerOrdersById(id)));
    }
}
