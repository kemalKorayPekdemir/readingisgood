package com.services.readingisgood.api.controller.book;

import com.services.readingisgood.api.constants.ApiEndpoints;
import com.services.readingisgood.api.request.book.BookRequest;
import com.services.readingisgood.api.response.base.ApiResponse;
import com.services.readingisgood.dto.book.BookDTO;
import com.services.readingisgood.service.book.BookMapper;
import com.services.readingisgood.service.book.BookService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping(ApiEndpoints.BOOK_API_URL)
@Validated
public class BookApiController {

    private final BookService bookService;

    @PostMapping(value = "/", consumes = {MediaType.APPLICATION_JSON_VALUE})
    @ApiOperation(value = "add new book to the store")
    public ApiResponse<BookDTO> addNewBook(@ApiParam(name = "request", type = "BookRequest",
            value = "new book request") @Valid @RequestBody BookRequest request) {
        return new ApiResponse<>(true, BookMapper.INSTANCE.bookToBookDTO(bookService.addNewBook(request)));
    }

    @PatchMapping(value = "/increasestock/{id}/{number}", consumes = {MediaType.ALL_VALUE})
    @ApiOperation(value = "increases the stock by the given number value")
    public ApiResponse<BookDTO> increaseStock(
            @ApiParam(name = "id", type = "String", value = "id of the book", required = true)
            @PathVariable("id") String id, @ApiParam(name = "number", type = "Integer",
            value = "increase the stock by the given number value", example = "1", required = true)
            @Min(value = 1) @PathVariable("number") Integer number) {
        return new ApiResponse<>(true, BookMapper.INSTANCE.bookToBookDTO(bookService.increaseStock(id, number)));
    }

    @GetMapping(value = "/{id}", consumes = {MediaType.ALL_VALUE})
    @ApiOperation(value = "get book by given id")
    public ApiResponse<BookDTO> getBookById(@ApiParam(name = "id", type = "String",
            value = "id of the book", required = true) @PathVariable("id") String id) {
        return new ApiResponse<>(true, BookMapper.INSTANCE.bookToBookDTO(bookService.getBookById(id)));
    }

    @GetMapping(value = "/", consumes = {MediaType.ALL_VALUE})
    @ApiOperation(value = "get all books")
    public ApiResponse<List<BookDTO>> getAllBooks() {
        return new ApiResponse<>(true, BookMapper.INSTANCE.bookListToBookDTOList(bookService.getAllBooks()));
    }
}
