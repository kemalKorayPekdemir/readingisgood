package com.services.readingisgood.api.controller.authentication;

import com.services.readingisgood.api.constants.ApiEndpoints;
import com.services.readingisgood.api.request.authentication.JwtRequest;
import com.services.readingisgood.api.response.authentication.JwtResponse;
import com.services.readingisgood.api.response.base.ApiResponse;
import com.services.readingisgood.service.authentication.helper.JwtTokenHelper;
import com.services.readingisgood.service.authentication.AuthenticationService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping(ApiEndpoints.AUTHENTICATION_API_URL)
public class AuthenticationController {

    private final JwtTokenHelper jwtTokenHelper;

    private final UserDetailsService userDetailsService;

    private final AuthenticationService authenticationService;

    @PostMapping
    @ApiOperation(value = "receives authentication token")
    public ApiResponse<JwtResponse> createAuthenticationToken(@ApiParam(name = "request", type = "JwtRequest",
            value = "authentication request") @Valid @RequestBody JwtRequest authenticationRequest) throws Exception {
        authenticationService.authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());
        return new ApiResponse<>(true, new JwtResponse(jwtTokenHelper.generateToken(userDetailsService
                .loadUserByUsername(authenticationRequest.getUsername()))));
    }

}