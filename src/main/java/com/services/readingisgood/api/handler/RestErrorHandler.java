package com.services.readingisgood.api.handler;

import com.services.readingisgood.api.response.base.ApiResponse;
import com.services.readingisgood.exception.ValidationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@ControllerAdvice(annotations = RestController.class)
public class RestErrorHandler {

    @ExceptionHandler(ValidationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ApiResponse exceptionHandler(ValidationException ex) {
        return new ApiResponse(false, null, ex.getRule().getCode(), ex.getRule().getDescription());
    }

}
