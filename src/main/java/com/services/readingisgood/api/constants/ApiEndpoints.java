package com.services.readingisgood.api.constants;

public class ApiEndpoints {

    public static final String API_BASE_URL = "/api";

    public static final String CUSTOMER_API_URL = API_BASE_URL + "/customers";

    public static final String ORDER_API_URL = API_BASE_URL + "/orders";

    public static final String BOOK_API_URL = API_BASE_URL + "/books";

    public static final String STATISTICS_API_URL = API_BASE_URL + "/statistics";

    public static final String AUTHENTICATION_API_URL = API_BASE_URL + "/authenticate";

}
