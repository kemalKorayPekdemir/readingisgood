package com.services.readingisgood.api.request.order;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@ApiModel
public class OrderItemRequest {

    @ApiModelProperty(required = true, value = "id of the book to be ordered", dataType = "List<String>")
    @NotNull
    private String bookId;

    @ApiModelProperty(required = true, value = "quantity", example = "1", dataType = "Integer")
    @Min(value = 1)
    @NotNull
    private Integer quantity;
}
