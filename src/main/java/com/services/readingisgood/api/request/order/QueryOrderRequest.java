package com.services.readingisgood.api.request.order;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
@ApiModel
public class QueryOrderRequest {

    @ApiModelProperty(required = true, value = "start date of the order", example = "2022-01-01",
            dataType = "LocalDate")
    @NotNull
    private LocalDate startDate;

    @ApiModelProperty(required = true, value = "end date of the order", example = "2022-05-01",
            dataType = "LocalDate")
    @NotNull
    private LocalDate endDate;
}
