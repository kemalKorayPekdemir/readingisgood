package com.services.readingisgood.api.request.customer;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@ApiModel
@Getter
@Setter
public class CustomerRequest {

    @ApiModelProperty(required = true, value = "First name of the customer", example = "name", dataType = "String")
    @NotBlank
    private String firstName;

    @ApiModelProperty(required = true, value = "Last name of the customer", example = "surname", dataType = "String")
    @NotBlank
    private String lastName;

    @ApiModelProperty(required = true, value = "Address of the customer", dataType = "AddressRequest")
    @Valid
    @NotNull
    private AddressRequest address;

    @ApiModelProperty(required = true, value = "Email of the customer", example = "email@mail.com", dataType = "String")
    @Email
    @NotBlank
    private String email;

    @ApiModelProperty(required = true, value = "Birth date of the customer", example = "1990-01-01",
            dataType = "LocalDate")
    @NotNull
    private LocalDate birthDate;
}
