package com.services.readingisgood.api.request.order;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@Getter
@Setter
@ApiModel
public class OrderRequest {

    @ApiModelProperty(required = true, value = "id of the customer", dataType = "String")
    @NotNull
    private String customerId;

    @ApiModelProperty(required = true, value = "id list of the books", dataType = "List<String>")
    @Valid
    @NotNull
    private List<OrderItemRequest> orderItemList;

}
