package com.services.readingisgood.api.request.book;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import nonapi.io.github.classgraph.json.Id;
import org.springframework.data.mongodb.core.index.Indexed;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Getter
@Setter
@ApiModel
public class BookRequest {

    @ApiModelProperty(required = true, value = "Name of the book", example = "book name", dataType = "String")
    @NotBlank
    private String bookName;

    @ApiModelProperty(required = true, value = "Name of the author of the book", example = "author name",
            dataType = "String")
    @NotBlank
    private String authorName;

    @ApiModelProperty(required = true, value = "price of the book", example = "100", dataType = "BigDecimal")
    @Min(value = 0)
    @NotNull
    private BigDecimal price;

    @ApiModelProperty(required = true, value = "stock of the book", example = "20", dataType = "Integer")
    @Min(value = 1)
    @NotNull
    private Integer stock;
}
