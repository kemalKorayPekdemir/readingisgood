package com.services.readingisgood.api.request.customer;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@ApiModel
public class AddressRequest {

    @ApiModelProperty(required = true, value = "Country of the customer", example = "Turkey", dataType = "String")
    @NotBlank
    private String country;

    @ApiModelProperty(required = true, value = "City of the customer", example = "Istanbul", dataType = "String")
    @NotBlank
    private String city;

    @ApiModelProperty(required = true, value = "District of the customer", example = "District", dataType = "String")
    @NotBlank
    private String district;

    @ApiModelProperty(required = true, value = "Street of the customer", example = "Street", dataType = "String")
    @NotBlank
    private String street;

    @ApiModelProperty(required = true, value = "Apartment number of the customer", example = "1",
            dataType = "Integer")
    @Min(value = 1)
    @NotNull
    private Integer apartmentNumber;

    @ApiModelProperty(required = true, value = "address of the customer", example = "adress", dataType = "String")
    @NotBlank
    private String address;


}
