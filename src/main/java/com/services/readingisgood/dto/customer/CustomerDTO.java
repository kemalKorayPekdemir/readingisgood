package com.services.readingisgood.dto.customer;

import com.services.readingisgood.domain.customer.Address;
import com.services.readingisgood.dto.order.OrderDTO;
import lombok.Data;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Data
public class CustomerDTO {

    private String id;

    private String firstName;

    private String lastName;

    private Address address;

    private String email;

    private LocalDate birthDate;

    List<OrderDTO> orderList = new ArrayList<>();

    private LocalDateTime createdDate;

    private LocalDateTime lastUpdatedDate;
}
