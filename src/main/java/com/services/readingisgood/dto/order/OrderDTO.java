package com.services.readingisgood.dto.order;

import com.services.readingisgood.enums.OrderStatus;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
public class OrderDTO {

    private String id;

    private String ownerFirstName;

    private String ownerLastName;

    private String ownerAddress;

    private List<OrderItemDTO> orderItemList;

    private OrderStatus orderStatus;

    private LocalDateTime createdDate;

    private LocalDateTime lastUpdatedDate;
}
