package com.services.readingisgood.dto.order;

import com.services.readingisgood.dto.book.BookDTO;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class OrderItemDTO {

    private String id;

    private BookDTO book;

    private Integer quantity;

    private LocalDateTime createdDate;

    private LocalDateTime lastUpdatedDate;
}
