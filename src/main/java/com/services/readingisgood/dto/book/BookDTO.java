package com.services.readingisgood.dto.book;

import lombok.Data;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
public class BookDTO {

    private String id;

    private String bookName;

    private String authorName;

    private BigDecimal price;

    private Integer stock;

    private LocalDateTime createdDate;

    private LocalDateTime lastUpdatedDate;
}
