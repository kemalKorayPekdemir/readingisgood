package com.services.readingisgood.domain.customer;

import com.services.readingisgood.domain.base.BaseDocument;
import com.services.readingisgood.domain.order.Order;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.DocumentReference;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Document
public class Customer extends BaseDocument {

    @NotNull
    private String firstName;

    @NotNull
    private String lastName;

    @NotNull
    private Address address;

    @Indexed(unique = true)
    @NotNull
    private String email;

    @NotNull
    private LocalDate birthDate;

    @DocumentReference()
    List<Order> orderList = new ArrayList<>();

    public void addOrder(Order order) {
        orderList.add(order);
    }
}
