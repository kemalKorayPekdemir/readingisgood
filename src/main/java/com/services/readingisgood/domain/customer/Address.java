package com.services.readingisgood.domain.customer;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class Address {

    @NotNull
    private String country;

    @NotNull
    private String city;

    @NotNull
    private String district;

    @NotNull
    private String street;

    @NotNull
    private Integer apartmentNumber;

    @NotNull
    private String address;

}
