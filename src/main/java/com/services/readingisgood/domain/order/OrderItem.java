package com.services.readingisgood.domain.order;

import com.services.readingisgood.domain.base.BaseDocument;
import com.services.readingisgood.domain.book.Book;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.DocumentReference;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@Document
@Builder
public class OrderItem extends BaseDocument {

    @NotNull
    @DocumentReference
    private Book book;

    @NotNull
    private Integer quantity;
}
