package com.services.readingisgood.domain.order;

import com.services.readingisgood.domain.base.BaseDocument;
import com.services.readingisgood.enums.OrderStatus;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.DocumentReference;

import javax.validation.constraints.NotNull;
import java.util.List;


@Getter
@Setter
@Document
@Builder
public class Order extends BaseDocument {

    @NotNull
    private String ownerFirstName;

    @NotNull
    private String ownerLastName;

    @NotNull
    private String ownerAddress;

    @NotNull
    @DocumentReference
    private List<OrderItem> orderItemList;

    @NotNull
    private OrderStatus orderStatus;

}
