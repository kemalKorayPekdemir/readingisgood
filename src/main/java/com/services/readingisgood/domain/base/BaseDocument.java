package com.services.readingisgood.domain.base;

import lombok.Getter;
import lombok.Setter;
import nonapi.io.github.classgraph.json.Id;

import java.time.LocalDateTime;

@Getter
@Setter
public class BaseDocument {

    @Id
    private String id;

    private LocalDateTime createdDate = LocalDateTime.now();

    private LocalDateTime lastUpdatedDate;
}
