package com.services.readingisgood.domain.book;

import com.services.readingisgood.domain.base.BaseDocument;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Getter
@Setter
@Document
public class Book extends BaseDocument {

    @NotNull
    private String bookName;

    @NotNull
    private String authorName;

    @NotNull
    private BigDecimal price;

    @NotNull
    private Integer stock;

    public void increaseStock(Integer number) {
        setStock(Integer.sum(stock, number));
    }
}
