FROM openjdk:11
MAINTAINER readingisgood
VOLUME /tmp
EXPOSE 8080
ADD build/libs/readingisgood-1.0.0.jar springbootdocker.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/springbootdocker.jar"]