# Readingisgood Application Documentation (Version 1.0.0)

## About The Application

Readingidgood is a simple book ordering application that includes following operations.

* Registering new customer
* Placing a new order
* Tracking the stock of books
* List all orders of the customer
* Viewing the order details
* Query monthly statistics of the customer

## Getting started

The application is written in Java 11 with spring boot framework and has 5 api controllers
with 13 restful endpoints. All operations are done through these endpoints. The application works on port 9393.

MongoDB is used as db. Mongo-express is used for managing db interactively and works on port 8081.

Swagger is used in this application and Swagger UI can be reached through '{host}:9393/swagger-ui/index.html' url.
Please download [Api Resource](postman-collection/readingisgood-api-resource.postman_collection.json) file and import it to the Postman for request examples.

Endpoints are secured with java web token. So that bearer token must be received through '{host/api/authenticate}' endpoint with the following credantials
* username : getir
* password : password

ValidationException(which is a RuntimeException) is used for specific error conditions.

### How To Start The Application

The application is containerized and can be executed via docker-compose. The following two commands are enough to start the application.

```
.\gradlew build
docker-compose up --build
```

#### Containers
There are three containers:

* readingisgoot-app (port 9393)
* mongodb (port 27017)
* mongodb-express (port 8081)

## Used Technologies

* Java 11
* Spring Boot (2.6.4)
* MongoDB
* Gradle (7.4)
* Docker

## Handled Exceptions

| Error Code | Message | Description |
|:--|---|------|
|ERR_BOOK_NOT_FOUND|Book is not found.|Book to order could not be found by given id|
|ERR_CUSTOMER_NOT_FOUND|This mail is already used.|Customer could not be found by given id|
|ERR_MAIL_ALREADY_USED|This mail is already used.|The given email has been used before|
|ERR_STOCK_INSUFFICIENT|Not enough items in the stock.| There is not enough quantity of the product to be ordered in stock|
|ERR_ORDER_NOT_FOUND|Order is not found.|Customer could not be found by given id|
|ERR_ORDER_ALREADY_DELIVERED|Order is already delivered.|Status cannot be promoted because the item has already been delivered|
